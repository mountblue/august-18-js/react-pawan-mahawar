import React, { Component } from "react";
import OpenChannel from "./OpenChannel";
import SendBird from "sendbird";
import OpenChannelList from "./OpenChannelList";
import OpneChannel from "./OpenChannel";
import uuid from "uuid";
import Hello from './HelloComponent';
import ChannelRoutes from "./ChannelRoutes"
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect
} from 'react-router-dom'

class OpenChannels extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sbd: new SendBird.getInstance(),
      appRef: this.props.appRef,
      user: "",
      currentChannel: null,
      openChannelList: this.props.openChannelList,
      openChannelListFlag: false,
      currentChannelFlag: false
    };
    this.handleCreateChannel = this.handleCreateChannel.bind(this);
    this.getOpenChannelList = this.getOpenChannelList.bind(this);
    this.getParticipantList = this.getParticipantList.bind(this);
  }

  componentWillMount() {
    this.getOpenChannelList();
  }

  getParticipantList() {
  }



  getOpenChannelList() {
    let openChannelList = this.state.openChannelList;
    let appRef = this.state.appRef;
    let that = this;
    appRef.child("openChannelList").on('value', function (snap) {
      let openChannelList = snap.val();
      that.setState({
        openChannelList: openChannelList
      });
    });

    this.state.openChannelListFlag = true;
  }

  handleStartChat(channel) {
    this.setState({ currentChannel: channel });
    this.props.handleStartChat(channel);

  }

  handleCreateChannel(event) {
    const chatName = this.refs.chatName.value;
    if (chatName === "") {
      alert("chat name is Required");
    } else {
      let openChannelList = this.state.openChannelList;
      let appRef = this.props.appRef;
      openChannelList.unshift(chatName);
      this.setState({
        openChannelList: openChannelList,
        currentChannel: chatName,
        currentChannelFlag: true
      });
      appRef.child("openChannelList").set(openChannelList);
    }
    event.preventDefault();
  }
  render() {
    let openChannelListFlag = this.state.openChannelListFlag;
    let currentChannelFlag = this.state.currentChannelFlag;
    let openChannelList;
    let currentChannel;
    if (openChannelListFlag) {
      openChannelList = (<div>
        <OpenChannelList
          startChat={this.handleStartChat.bind(this)}
          openChannels={this.state.openChannelList}
        />
        {/* <ChannelRoutes openChannels={this.state.openChannelList} /> */}
      </div>
      );
    } else {
      openChannelList = <p>No opened channel</p>;
    }
    if (currentChannelFlag) {
      currentChannel = (
        <OpenChannel
          startChat={this.handleStartChat.bind(this)}
          key={uuid.v4()}
          channel={this.state.currentChannel}
        />

      );
    } else {
      currentChannel = <p> No Current Channel Selected</p>;
    }

    return (
      // <Router>
      <div>
        <div className="user">
          <div>
            <form onSubmit={this.handleCreateChannel}>
              <div>
                <input id="channelInput"
                  class="input is-small"
                  type="text"
                  ref="chatName"
                  placeholder="create channel"
                />
                <button class="button is-small is-fullwidth is-dark" type="submit" value="submit" >Add channel</button>
              </div>
            </form>
          </div>
        </div>
        <div class="block">
          <h6 class="title is-6"> #Open Channel</h6>
          {openChannelList}
        </div>

      </div>
    );
  }
}

export default OpenChannels;
