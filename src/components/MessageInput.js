import React, { Component } from "react";
import { Navbar } from 'react-bootstrap';
import { KEY_ENTER } from '../const';
import moment from 'moment';
import Messages from "./Messages";

class MessageInput extends Component {
  constructor(props) {
    super(props)
    this.state = {
      value: "enter massage"
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    let value = this.refs.inputMessage.value;
    if (event.keyCode === KEY_ENTER && value.length !== 0) {
      let tm = moment().format('llll');
      let massage = { "msg": value, "tm": tm };
      this.props.handleMessage(massage);
      this.refs.inputMessage.value = "";
    }

  }
  render() {
    return (<div>
      <Navbar className="navbar-fixed-bottom">

        <input
          onKeyUp={this.handleSubmit}
          id="message-input"
          type="text"
          placeholder={this.state.value}
          ref="inputMessage"
        />

      </Navbar>
    </div>
    );
  }
}

export default MessageInput;
