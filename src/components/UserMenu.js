import React, { Component } from "react";
import OpenChannels from "./OpenChannels";
import GroupChat from "./GroupChat";
import { SendBirdAction } from "../SendBirdAction";
import Users from './Users';
import StatusModal from "./StatusModal"
class UserMenu extends Component {
  constructor(props) {
    super(props);
    let userInfo = this.props.userInfo;
    const sbObj = new SendBirdAction();
    this.state = {
      sb: sbObj,
      user: "",
      appRef: this.props.appRef,
      userInfo: this.props.userInfo,
      userid: userInfo.userid,
      nickname: userInfo.nickname,
      openChannelList: [],
      groupChannelList: [],
      statusFlag: false
    };
    this.getConnect = this.getConnect.bind(this);
    this.setStatus = this.setStatus.bind(this);
  }

  componentWillMount() {
    this.getConnect();
  }

  getConnect() {
    const userid = this.state.userid;
    const nickname = this.state.nickname;
    const sb = this.state.sb;
    // let appRef = this.state.appRef;
    // appRef.child(`users/${userInfo.userid}`).set(userInfo)
    sb.connect(
      userid,
      nickname
    )
      .then(user => {
        this.setState({ user: user });
      })
      .catch(err => {
        console.log(err);
      });
  }
  setStatus() {
    this.setState({
      statusFlag: true
    })
  }
  getStatusList() {
    if (this.state.statusFlag) {
      return (

        // <div>
        //   <StatusModal >
        //     <p>pawan</p>
        //   </StatusModal>
        // </div>
        <div class="">
          <select >
            <option defaultValue value="At Work">At Work</option>
            <option value="in a Meeting">in a Meeting</option>
            <option value="Commuting">Commuting</option>
            <option value="Out Sick">Out Sick</option>
            <option value="Vacationing">Vacationing</option>
            <option value="Working remotely">Working remotely</option>
          </select>
        </div>
      )
      this.setState({
        statusFlag: false
      });
    }

  }
  handleStartChat(channel) {
    let userObj = {};
    userObj["channel"] = channel;
    userObj["userImg"] = this.state.user.profileUrl;
    this.props.handleStartChat(userObj);
  }
  handleStartChatWithUser(user) {
    let userObj = {};
    userObj["user"] = user;
    userObj["userImg"] = this.state.user.profileUrl;
    this.props.handleStartChatWithUser(userObj)
  }

  render() {
    return (
      <aside class="menu">
        <div class="blokc">
          <div class="box">
            <h4 class="title is-6"> #MySpace</h4>
            <p onClick={this.setStatus}><img id="userImage" src={this.state.user.profileUrl} alt="" />{this.state.nickname}</p>
            {this.getStatusList()}
          </div>
        </div>
        <div class="panel list-group">
          <OpenChannels
            openChannelList={this.state.openChannelList}
            user={this.state.user}
            userInfo={this.state.userInfo}
            handleStartChat={this.handleStartChat.bind(this)}
            appRef={this.state.appRef}
          />
        </div>
        <div class="block">
          <h6 class="title is-6"> #Direct Message</h6>
          <Users
            appRef={this.state.appRef}
            userInfo={this.state.userInfo}
            handleStartChat={this.handleStartChatWithUser.bind(this)}
          />
        </div>
        <GroupChat groupChatList={this.state.groupChatList} />
      </aside >
    );
  }
}

export default UserMenu;
