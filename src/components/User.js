import React, { Component } from 'react';

class User extends Component {
    startChat(user) {
        this.props.startChat(user);
    }
    render() {
        return (
            <div
                onClick={this.startChat.bind(this, this.props.user)}
                class="panel-block list-group-item">  {this.props.user.nickname}
            </div>

        );
    }
}
export default User;