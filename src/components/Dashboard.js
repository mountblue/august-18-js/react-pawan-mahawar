import React, { Component } from 'react';
import MessageBox from './MessageBox';
import UserMenu from './UserMenu'
import { SendBirdAction } from "../SendBirdAction";
import SendBird from "sendbird";
import {
    BrowserRouter as Router,
    Route,
    Link,
    Switch,
    Redirect
} from 'react-router-dom'

class Dashborad extends Component {
    constructor(props) {
        super(props);
        const sbd = new SendBird.getInstance();
        let userInfo = this.props.userInfo;

        this.state = {
            sbd: sbd,
            userInfo: this.props.userInfo,
            userid: userInfo.userid,
            nickname: userInfo.nickname,
            appRef: this.props.appRef,
            selectedChannel: [],
            openChannelList: ['channel12', 'channel10'],
            groupChannelList: [],
            personalMessageFlag: false,
            userImg: "",
            currentChannel: "",
            messageHistory: {
                channel10: [],
                channel12: [],

            }
        };
    }

    // componentWillMount() {
    //     // let messageHistory = this.state.messageHistory;
    //     // let appRef = this.props.appRef;
    //     // appRef.child("messageHistory").set(messageHistory);
    //     // appRef.child("openChannelList").set(this.state.openChannelList);
    // }

    componentWillMount() {
        let messageHistory = this.state.messageHistory;
        let appRef = this.props.appRef;
        let that = this;
        // appRef.child("messageHistory").set(messageHistory);
        appRef.child("openChannelList").set(this.state.openChannelList);
        appRef.child("messageHistory").once("value", function (snap) {
            let messageHistory = snap.val();
            if (messageHistory) {
                that.setState({ messageHistory });
            }
            console.log(messageHistory)
        });
    }


    messageHandler(message, channelName) {
        let personalMessageFlag = this.state.personalMessageFlag;
        let that = this;
        let appRef = this.props.appRef;
        let messageHistory = this.state.messageHistory;
        let message_id1;
        let message_id2;
        if (personalMessageFlag) {
            let user = this.state.nickname;
            message_id1 = channelName + user;
            message_id2 = user + channelName;

            let userMsg = messageHistory[message_id1];
            let otherMsg = messageHistory[message_id2];
            if (userMsg === undefined) {
                userMsg = [];
            }
            if (otherMsg === undefined) {
                otherMsg = [];
            }
            userMsg.push(message);
            otherMsg.push(message);
            messageHistory[message_id1] = userMsg;
            messageHistory[message_id2] = otherMsg;

        } else {
            let channelMessages = messageHistory[channelName];
            if (channelMessages === undefined) {
                channelMessages = [];
            }
            channelMessages.push(message);
            messageHistory[channelName] = channelMessages;
        }
        this.setState({
            messageHistory
        })
        appRef.child("messageHistory").set(messageHistory);
        appRef.child("messageHistory").on('value', function (snap) {
            let messageHistory = snap.val();
            that.setState({
                messageHistory
            });
        })
    }

    handleStartChat(userObj) {
        let appRef = this.state.appRef;
        let that = this;
        this.setState({
            currentChannel: userObj.channel,
            personalMessageFlag: false,
            userImg: userObj.userImg
        })
        let messageHistory = this.state.messageHistory;
        console.log(messageHistory)
        appRef.child("messageHistory").on("value", function (snap) {
            messageHistory = snap.val();
            if (messageHistory) {
                that.setState({ messageHistory });
            }
            console.log(messageHistory)
        });

    }
    handleStartChatWithUser(userObj) {
        let user = userObj.user
        let that = this;
        let appRef = this.state.appRef;
        this.setState({
            currentChannel: user.nickname,
            personalMessageFlag: true,
            userImg: userObj.userImg
        });
        let messageHistory = this.state.messageHistory;
        console.log(messageHistory)
        appRef.child("messageHistory").on("value", function (snap) {
            messageHistory = snap.val();
            if (messageHistory) {
                that.setState({ messageHistory });
            }
            console.log(messageHistory)
        });
    }
    render() {

        let currentChannel = this.state.currentChannel;
        let messageBox;
        let channelName = currentChannel;
        let messageHistory
        if (this.state.personalMessageFlag) {
            messageHistory = this.state.messageHistory;
        }
        else {
            messageHistory = this.state.messageHistory;
        }
        if (currentChannel) {
            messageBox = (<MessageBox
                nickname={this.state.nickname}
                userid={this.state.userid}
                currentChannel={currentChannel}
                appRef={this.state.appRef}
                channelName={channelName}
                messageHistory={messageHistory}
                handleMessage={this.messageHandler.bind(this)}
                userImg={this.state.userImg}
                personalMessageFlag={this.state.personalMessageFlag}
            />)


        } else {
            messageBox = <MessageBox
                handleMessage={this.messageHandler.bind(this)}
            />
        }

        return (
            <Router>

                < div id="dashboard" >
                    {/* <Link to="/hello"> hello
                    </Link>

                    <Route path="/hello" render={(props) => <Hello {...props} user={"pawan mahawar"} />} /> */}
                    {/* <Route path="/channel*" render={(props) => <Hello {...props} user={"pawan mahawar"} />} />  */}
                    < UserMenu
                        handleStartChat={this.handleStartChat.bind(this)}
                        handleStartChatWithUser={this.handleStartChatWithUser.bind(this)}
                        userInfo={this.state.userInfo}
                        appRef={this.state.appRef}
                    />
                    {messageBox}

                </div >
            </Router >
        );
    }
}

export default Dashborad;
