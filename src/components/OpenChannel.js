import React, { Component } from "react";
import uuid from "uuid";


class OpenChannel extends Component {
  startChat(channel) {
    this.props.startChat(channel);
  }
  render() {

    return (
      <div
        key={uuid.v4()}
        onClick={this.startChat.bind(this, this.props.channel)}
        class="panel-block list-group-item">
        {this.props.channel}

      </div >
    );
  }
}

export default OpenChannel;
