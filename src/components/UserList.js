import React, { Component } from 'react';
import User from './User';
import uuid from "uuid";
import { Link } from "react-router-dom";

class UserList extends Component {
    constructor(props) {
        super(props)
        this.state = {
        };
    }

    handleStartChat(user) {
        this.props.startChat(user)
    }
    render() {
        let users = this.props.users
        return (
            <div className="UserList">
                {
                    Object.keys(users).map(user => {
                        return (
                            <Link to={`/dashboard/${users[user].nickname}`} key={uuid.v4()}>
                                <User
                                    startChat={this.handleStartChat.bind(this)}
                                    key={uuid.v4()}
                                    user={users[user]}
                                />
                            </Link>
                        )

                    })

                }
            </div>);
    }
}

export default UserList;