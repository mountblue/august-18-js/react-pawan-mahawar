import React, { Component } from 'react';
import {
    Route,
    Link,
} from 'react-router-dom'

class Header extends Component {
    render() {
        return (
            <nav class="navbar is-dark " >
                <div class="navbar-brand">
                    <div class="navbar-start">
                        <Link to="/home" class="navbar-item" href="">
                            Home
                        </Link>
                        <Link to="/dashboard" class="navbar-item" href="">
                            Dashborad
                        </Link>
                    </div>
                </div>
                {/* <div class="is-divider-vertical" data-content="OR"></div> */}
                <div class="navbar-end">
                    <Link to="/login" class="navbar-item" href="">
                        Login
                          </Link>

                    <Link to="/singup" class="navbar-item" href="">
                        Sing up
                        </Link>
                </div>
            </nav>

        );
    }
}

export default Header;