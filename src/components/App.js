import React, { Component } from 'react';
import Dashboard from './Dashboard';
import "../css/main.css";
import Login from './Login';
import * as firebase from 'firebase';
import { DB_CONFIG } from '../firebaseconfig';
import Header from "./Header";
import Home from "./Home";
import Singup from "./Singup";
import StatusModal from './StatusModal';
import {
    BrowserRouter as Router,
    Route,
    Link,
    Switch,
    Redirect
} from 'react-router-dom'

firebase.initializeApp(DB_CONFIG);

class App extends Component {
    constructor() {

        super();
        this.state = {
            appRef: "",
            isLogin: true,
            userInfo: { userid: "pawan", nickname: "bruce" },
        }
        this.handleLogin = this.handleLogin.bind(this);
    }
    componentWillMount() {
        let appRef = firebase.database().ref("App")
        this.setState({
            appRef
        });
    }
    handleLogin(userInfo) {
        let appRef = this.state.appRef;
        appRef.child(`users/${userInfo.userid}`).set(userInfo)
        this.setState({
            userInfo,
            isLogin: true
        })
    }
    render() {

        return (
            <Router>
                <div>
                    <Header />
                    <Route path="/home" exact strict component={Home} />
                    <Route path="/singup" exact strict component={Singup} />
                    <Route path="/login" exact strict render={(props) => <Login {...props}
                        login={this.handleLogin}
                    />} />
                    <Route exact strict path="/dashboard" render={(props) => (this.state.isLogin ? (<Dashboard {...props}
                        userInfo={this.state.userInfo}
                        appRef={this.state.appRef}
                    />) : (<Redirect to="/login" />))} />


                    <Route path="/" render={() => (
                        this.state.isLogin ? (
                            <Redirect to="/dashboard" />
                        ) : (
                                <Redirect to="/login" />
                            )
                    )} />
                    {/* <Route path="/dashboard" render={() => (
                        this.state.isLogin ? (
                            <Redirect to="/dashboard" />
                        ) : (
                                <Redirect to="/login" />
                            )
                    )} /> */}


                </div>
            </Router >

        )
    }
}

export default App;