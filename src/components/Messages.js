import React, { Component } from 'react';
import { Navbar, Nav, NavItem } from 'react-bootstrap';
import uuid from "uuid";

class Messages extends Component {

    render() {
        let channelInfo = this.props.channelInfo;
        let messageHistory = this.props.messageHistory;
        let Usermessages;
        let channelName;
        let user = this.props.nickname;
        let counter = 0;
        if (this.props.personalMessageFlag) {
            channelName = this.props.nickname + this.props.channelName;

        } else {
            channelName = this.props.channelName;
        }
        // console.log(channelName)
        if (messageHistory !== undefined) {
            // console.log((channelName in messageHistory))
        }
        if (messageHistory !== undefined && channelName in messageHistory) {
            console.log(messageHistory);
            Usermessages = messageHistory[channelName].map(usr => {
                let user = Object.keys(usr)[0]
                if (usr[user] !== undefined) {
                    let userImg = this.props.userImg;
                    let msg;
                    counter++;
                    if (counter % 2 == 0) {
                        msg = (<div className="container darker" key={uuid.v4()} >
                            <img src={`${userImg}`} alt="Avatar" style={{ width: '28px' }}></img>
                            {/* <image src={`${userImg}`} alt="Avatar"></image> */}
                            <p> {usr[user].msg}</p>
                            <span class="time-right">{usr[user].tm}</span>
                            <span class="time-left">{user}</span>
                        </div >)
                    } else {
                        msg = (<div className="container" key={uuid.v4()}>
                            <img src={`${userImg}`} alt="Avatar" style={{ width: '28px' }}></img>
                            {/* <image src={`${userImg}`} alt="Avatar"></image> */}
                            <p> {usr[user].msg}</p>
                            <span class="time-right">{usr[user].tm}</span>
                            <span class="time-left">{user}</span>
                        </div >)
                    }

                    return (msg)
                }

            })

        }

        return (
            <div id="message-box-child">
                {/* <div>
                    <Navbar className="navbar-fixed-top">
                        #{this.props.channelName}
                    </Navbar>
                </div> */}
                <div>
                    {Usermessages}
                </div>
            </div>
        );

    }
}

export default Messages;