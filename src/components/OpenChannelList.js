import React, { Component } from "react";
import OpenChannel from "./OpenChannel";
import uuid from "uuid";
import { Link, Route } from "react-router-dom";

class OpenChannelList extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }
  handleStartChat(channel) {
    this.props.startChat(channel)
  }
  render() {
    let channel = this.props.channel;
    return (

      <div>
        {this.props.openChannels.map(openChannel => {
          return (

            <Link to={`/dashboard/${openChannel}`} key={uuid.v4()}>
              <OpenChannel
                startChat={this.handleStartChat.bind(this)}
                channel={openChannel}
              />
            </Link>
          );
        })}

      </div>

    );
  }
}

export default OpenChannelList;
