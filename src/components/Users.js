import React, { Component } from 'react';
import UserList from './UserList'

class Users extends Component {
    constructor(props) {
        super(props)
        this.state = {
            appRef: this.props.appRef,
            userList: [],
            userListFlag: true,
            userInfo: this.props.userInfo
        };

        this.getUserList = this.getUserList.bind(this);
    }
    componentWillMount() {
        this.getUserList();
    }
    getUserList() {
        let userList = this.state.userList;
        let appRef = this.state.appRef;
        let that = this;
        appRef.child("users").on('value', function (snap) {
            let userList = snap.val();
            that.setState({
                userList,
                userListFlag: true
            });
            console.log(userList)
        });

    }
    handleStartChat(user) {
        // this.setState({ currentChannel:  });
        this.props.handleStartChat(user);

    }
    render() {
        let userList;
        if (this.state.userListFlag) {
            userList = (<UserList
                startChat={this.handleStartChat.bind(this)}
                userInfo={this.state.userInfo}
                users={this.state.userList}
                appRef={this.state.appRef}
            />)
        }

        return (
            <div className="UserList">
                {userList}
            </div>);
    }
}

export default Users;