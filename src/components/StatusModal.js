import React, { Component } from 'react';
import ReactDOM from 'react-dom'

class StatusModal extends Component {

    componentDidMount() {
        this.modalTarget = document.createElement('div');
        this.modalTarget.className = 'modal';
        document.body.appendChild(this.modalTarget);
        this._render();
    }
    componentWillUpdate() {
        this._render();
    }
    componentWillUnmount() {
        ReactDOM.unmountComponentAtNode(this.modalTarget);
        document.body.removeChild(this.modalTarget);
    }
    _render() {
        ReactDOM.render(
            <div>{this.props.children}</div>,
            this.modalTarget
        );
    }
    render() {
        return (
            <div >
                {/* <i class="close icon"></i>
                <div class="header">
                    Profile Picture
                </div>
                <div class="image content">
                    <img src="/images/avatar/large/chris.jpg" />
                </div>
                <div class="description">
                    <div class="ui header">We've auto-chosen a profile image for you.</div>
                    <p>We've grabbed the following image from the <a href="https://www.gravatar.com" target="_blank">gravatar</a> image associated with your registered e-mail address.</p>
                    <p>Is it okay to use this photo?</p>
                </div>
                <div class="actions">
                    <div class="ui black deny button">
                        Nope
                     </div>
                    <div class="ui positive right labeled icon button">
                        Yep, that's me
                      <i class="checkmark icon"></i>
                    </div>
                </div> */}
            // {/* <div class="select is-multiple">
            //   <select >
            //     <option selected value="At Work">At Work</option>
            //     <option value="in a Meeting">in a Meeting</option>
            //     <option value="Commuting">Commuting</option>
            //     <option value="Out Sick">Out Sick</option>
            //     <option value="Vacationing">Vacationing</option>
            //     <option value="Working remotely">Working remotely</option>
            //   </select>
            // </div> */}
            </div>
        );
    }
}

export default StatusModal;