import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Route,
    Link,
    Switch,
    Redirect
} from 'react-router-dom'


class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userid: "",
            nickname: ""
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(event) {
        let targetName = event.target.name;
        let value = event.target.value;
        if (targetName === "userid") {
            this.setState({
                userid: value
            })
        } else {
            this.setState({
                nickname: value
            })
        }
    }

    handleSubmit(event) {
        if (this.state.userid === "" || this.state.nickname === "") {
            console.log(" enter the values")

        } else {
            let userInfo = {}
            userInfo["userid"] = this.state.userid;
            userInfo["nickname"] = this.state.nickname;
            this.props.login(userInfo);
        }
        event.preventDefault();
    }

    render() {
        return (
            <div id="login-div">
                <form onSubmit={this.handleSubmit}>
                    <input type="text" class="input" name="userid" placeholder="userid" value={this.state.userid} onChange={this.handleChange} /><br />
                    <input type="text" class="input" name="userName" placeholder="user name" value={this.state.nickname} onChange={this.handleChange} /><br />
                    <input type="submit" class="ui-button" value="Submit" />
                </form >
            </div>
        );
    }
}

export default Login;
