import React, { Component } from 'react';
import Messages from './Messages';
import MessageInput from './MessageInput'
import { Navbar, Nav, NavItem } from 'react-bootstrap';
import {
    BrowserRouter as Router,
    Route,
    Link,
    Switch,
    Redirect
} from 'react-router-dom'

class MessageBox extends Component {
    constructor(props) {
        super(props)
        this.state = {
            currentChannel: this.props.channelName,
            appRef: this.props.appRef
        }
    }
    handleMessage(messageObj) {
        let nickname = this.props.nickname;
        let message = {};
        message[nickname] = messageObj;
        let channelName = this.props.channelName
        if (!channelName) {
            console.log("please select the channel");
        }
        else {
            this.props.handleMessage(message, channelName);
        }

    }
    render() {
        return (
            <div id="message-box">
                <div>
                    <Route path={`/dashboard/${this.props.channelName}`} render={(props) => <Messages {...props}
                        userid={this.props.userid}
                        channelName={this.props.channelName}
                        messageHistory={this.props.messageHistory}
                        nickname={this.props.nickname}
                        personalMessage={this.state.personalMessage}
                        userImg={this.props.userImg}
                        personalMessageFlag={this.props.personalMessageFlag}
                    />} />
                </div>
                <MessageInput
                    handleMessage={this.handleMessage.bind(this)}
                />
            </div>
        );
    }
}

export default MessageBox;